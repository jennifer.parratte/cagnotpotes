<?php

namespace App\Controller;

use App\Entity\Payment;
use App\Form\PaymentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Participant;
use App\Entity\Campaign;

/**
 * @Route("/payment")
 */
class PaymentController extends AbstractController
{
    /**
     * @Route("/charge", name="payment_charge", methods="POST")
     */
    public function charge(Request $request): Response
    {

        $campaign_id = $request->request->get('campaign_id');

        $amount = (int)$request->request->get("amount") * 100;
        ;

    try {
            //€ffectuer le paiement, ne pas continuer la fonction si on échoue
            \Stripe\Stripe::setApiKey('sk_live_dnO6msejvzwsuhBnXXPW98ie');
            $charge = \Stripe\Charge::create(['amount' => $amount, 'currency' => 'eur', 'source' => $request->request->get("stripeToken")]);
        }

    catch(\Exception $e) {
        $this->addFlash(
            'error',
            'Lepaiement à échoué. Raison : '. $e->getMessage()
        );
        return $this->redirectToRoute('campaign_pay', [
            "id" => $campaign_id
        ]);
        //TODO: Gérer les messages spéciaux
    }
            //Enregistrer le participant
            $participant = new Participant();

            $participant->setName($request->request->get("name"));
            $participant->setEmail($request->request->get("email"));
            
            $campaign = $this->getDoctrine()->getRepository(Campaign::class)->find($campaign_id);
            $participant->setCampaign($campaign);

            //sert a enregistrer les données de participant
            $em = $this->getDoctrine()->getManager();
            $em->persist($participant);
            $em->flush();

            //Enregistrer le payment qui est dépendant du participant
            $payment = new Payment();
            $payment->setParticipant($participant);
            $payment->setAmount($amount);

            //$payment->setParticipantId($participant->getId());

            $em = $this->getDoctrine()->getManager();
            $em->persist($payment);
            $em->flush();

            //Redirection vers la fiche campagne
            return $this->redirectToRoute('campaign_show', [
                "id" => $campaign_id
            ]);
    }
}
