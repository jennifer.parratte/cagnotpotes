<?php

namespace App\Controller;

use App\Entity\Spending;
use App\Form\SpendingType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Campaign;
use App\Entity\Participant;

/**
 * @Route("/spending")
 */
class SpendingController extends AbstractController
{
    /**
     * @Route("/{id}/spending", name="spending_index", methods="GET")
     */
    public function index(Campaign $campaign): Response
    {
        $spendings = $this->getDoctrine()
            ->getRepository(Spending::class)
            ->findAll();

        return $this->render('spending/index.html.twig',compact("campaign"));
    }



    /**
     * @Route("/new", name="spending_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        //Enregistrer le participant
        $participant = new Participant();

        $campaign_id = $request->request->get('campaign_id');

        $participant->setName($request->request->get("name"));
        $participant->setEmail($request->request->get("email"));

        $campaign = $this->getDoctrine()->getRepository(Campaign::class)->find($campaign_id);

        $participant->setCampaign($campaign);

        $em = $this->getDoctrine()->getManager();
        $em->persist($participant);
        $em->flush();

        $spending = new Spending();
        $amount = (int)$request->request->get('amount')*100;
        $label = $request->request->get('product');

        $spending->setAmount($amount);
        $spending->setParticipant($participant);
        $spending->setLabel($label);

       
        $em = $this->getDoctrine()->getManager();
        $em->persist($spending);
        $em->flush();

        return $this->redirectToRoute('campaign_show', [
            'id' => $request->request->get('campaign_id')
        ]);
    }

    /**
     * @Route("/{idspending}", name="spending_show", methods="GET")
     */
    public function show(Spending $spending): Response
    {
        return $this->render('campaign/show.html.twig', ['spending' => $spending]);
    }

    /**
     * @Route("/{idspending}/edit", name="spending_edit", methods="GET|POST")
     */
    public function edit(Request $request, Spending $spending): Response
    {
        $form = $this->createForm(SpendingType::class, $spending);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('spending_edit', ['idspending' => $spending->getIdspending()]);
        }

        return $this->render('spending/edit.html.twig', [
            'spending' => $spending,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idspending}", name="spending_delete", methods="DELETE")
     */
    public function delete(Request $request, Spending $spending): Response
    {
        if ($this->isCsrfTokenValid('delete'.$spending->getIdspending(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($spending);
            $em->flush();
        }

        return $this->redirectToRoute('spending_index');
    }
}
